package com.ex.school;

public class Student extends com.ex.school.Person {


    private double examGrade;

    public Student(Long cnp, String firstName, String lastName, int age, double examGrade) {
        super(cnp, firstName, lastName, age);
        this.examGrade = examGrade;
    }


    public double getExamGrade() {
        return examGrade;
    }

    public void setExamGrade(double examGrade) {
        this.examGrade = examGrade;
    }

}

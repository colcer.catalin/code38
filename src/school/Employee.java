
    package com.ex.school;

// extends răspunde/e analog la "is a" ex: Teacher is a Person
public class Employee extends com.ex.school.Person {

    private String occupation;

    public Employee(Long cnp, String firstName, String lastName, int age, String occupation) {
        // super apelează constructorul SUPERclasei Person
        super(cnp, firstName, lastName, age);
        this.occupation = occupation;
    }

    public Employee(Long cnp, String occupation) {
        // super apelează constructorul SUPERclasei Person
        super(cnp);
        this.occupation = occupation;
    }
}
